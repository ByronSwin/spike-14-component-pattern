#include "StateCommand.h"

using namespace std;

/*
	State command, inherits from command. This command has a special role; telling the context controller to change the current state.
	
	@param ids vector string for ids for this command
*/
StateCommand::StateCommand(vector<string> ids)
: Command(ids)
{
}

/*
	Handles the formatted input, setting the state to the variable

	@param input the formatted, split input.
*/
void StateCommand::Update(vector<string> input)
{
	if (input.at(1).compare("START") == 0) //Start and select mean the same things, so is converted to select
		_state = "SELECT";
	else
		_state = input.at(1);
}

/*
	Getter for the state variable

	@return string the state the user has requested
*/
string StateCommand::State()
{
	return _state;
}

StateCommand::~StateCommand()
{
}
