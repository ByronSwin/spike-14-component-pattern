#include "GameplayState.h"
#include "LookCommand.h"
#include "GoCommand.h"
#include "TakeCommand.h"


using namespace std;

static bool firstDisplay; // Determines if this is the first time the game is showing

/*
	The main game state and the most complex. Uses the game data and handles all in game controls

	Constructor takes in a pointer to the game, as well as ids for this state.

	A default and display string variable is used to customise the output based on handled ommands

	@param ids the ids for this state
	@param theGame pointer to the game data.
*/
GameplayState::GameplayState(vector<string> ids, GameData* theGame)
: State(ids)
{
	_theGame = theGame;
	firstDisplay = true;
}

/*
	Gets the input for the user, adding the word gameplay behind it so that the command processor knows what state the command is from

	@returns string the users' modified unformatted input
*/
string GameplayState::GetInput()
{
	string result = "GAMEPLAY ";
	string input;
	getline(cin, input);
	result += input;

	return result;
}

/*
	Handles the command passed in. This method handles each type of command differntly. Based on each command, the display string is modified. The default is regularly refreshed.
*/
void GameplayState::Handle(Command* cmd)
{
	_displayString = "";
	RefreshDefault();

	/*
		The look command handeling. If there is an error, display that and continue, otherwise find what the user wants to look at (if it exists) and display its long description.
	*/
	if (typeid(*cmd) == typeid(LookCommand))
	{
		InventoryComponent* locInv = dynamic_cast<InventoryComponent*>(_theGame->GetPlayer()->GetLocation()->GetComponent("INVENTORY"));
		InventoryComponent* playerInv = dynamic_cast<InventoryComponent*>(_theGame->GetPlayer()->GetComponent("INVENTORY"));
		LookCommand* c = dynamic_cast<LookCommand*>(cmd);
		if (c->getErrMsg().compare("NOERROR") != 0)
		{
			_displayString = c->getErrMsg();
		}
		else
		{
			if (c->getAtId().compare("ME") == 0 ||
				c->getAtId().compare("PLAYER") == 0)
			{
				_displayString = _theGame->GetPlayer()->LongDesc();
			}
			else if (c->getAtId().compare("AROUND") == 0 || c->getAtId().compare("ROOM") == 0
				|| _theGame->GetPlayer()->GetLocation()->AreYou(c->getAtId()))
			{
				_displayString = "You are in " + _theGame->GetPlayer()->GetLocation()->LongDesc();
				_displayString += "\nYou can see paths: ";
				_displayString += _theGame->GetCurrPaths();
			}
			else if (c->getInId().compare("ROOM") == 0 ||
				c->getInId().compare("AROUND") == 0 ||
				_theGame->GetPlayer()->GetLocation()->AreYou(c->getInId()))
			{
				if (locInv->HasEntity(c->getAtId()))
				{
					_displayString = locInv->GetEntity(c->getAtId())->LongDesc();
				}
				else if (playerInv->HasEntity(c->getAtId()))
				{
					_displayString = playerInv->GetEntity(c->getAtId())->LongDesc();
				}
				else 
				{ 
					_displayString = "You cannot see " + c->getAtId() + " in the " + c->getInId() + ".\n";
				}
			}
			else if (locInv->HasEntity(c->getInId()))
			{
				Entity* inEntity = locInv->GetEntity(c->getInId());
				if (inEntity->HasComponent("INVENTORY"))
				{
					InventoryComponent* inInv = dynamic_cast<InventoryComponent*>(inEntity->GetComponent("INVENTORY"));
					if (inInv->HasEntity(c->getAtId()))
					{
						_displayString = inInv->GetEntity(c->getAtId())->LongDesc();
					}
					else
					{
						_displayString = c->getAtId() + " isnt in the " + c->getInId();
					}

				}
				else
				{
					_displayString = c->getInId() + " does not have an inventory.";
				}

			}
			else if (playerInv->HasEntity(c->getInId()))
			{
				Entity* inEntity = playerInv->GetEntity(c->getInId());
				if (inEntity->HasComponent("INVENTORY"))
				{
					InventoryComponent* inInv = dynamic_cast<InventoryComponent*>(inEntity->GetComponent("INVENTORY"));
					if (inInv->HasEntity(c->getAtId()))
					{
						_displayString = inInv->GetEntity(c->getAtId())->LongDesc();
					}
					else
					{
						_displayString = c->getAtId() + " isnt in the " + c->getInId();
					}

				}
				else
				{
					_displayString = c->getInId() + " does not have an inventory.";
				}

			}
			else
			{
				_displayString = "You cannot see " + c->getInId();
			}

		}

	}

	/*
		If the command is a take command, check for error. If no error, find where the player wants to take the item from and what item it is that they wont.
		If an item is located, make sure it is carrayable (note, if the player is carrying an item with an inventory, it is assumed all items within that inventory are also
		carryable).

		If all these tests are passed, item is placed into the players inventory. The dispaly string is updated to explain what happened or what the issue was
	*/
	else if (typeid(*cmd) == typeid(TakeCommand))
	{
		InventoryComponent* locInv = dynamic_cast<InventoryComponent*>(_theGame->GetPlayer()->GetLocation()->GetComponent("INVENTORY"));
		InventoryComponent* playerInv = dynamic_cast<InventoryComponent*>(_theGame->GetPlayer()->GetComponent("INVENTORY"));
		TakeCommand* c = dynamic_cast<TakeCommand*>(cmd);
		if (c->GetErrMsg().compare("NOERROR") != 0)
		{
			_displayString = c->GetErrMsg();
		}
		else
		{
			if (c->GetFromId().compare("ROOM") == 0 || 
				_theGame->GetPlayer()->GetLocation()->AreYou(c->GetFromId()))
			{
				if (locInv->HasEntity(c->GetTakeId()))
				{
					if (locInv->GetEntity(c->GetTakeId())->HasComponent("CARRYABLE"))
					{
						playerInv->AddEntity(locInv->TakeEntity(c->GetTakeId()));
						_displayString = c->GetTakeId() + " was added to your inventory!";
					}
					else
					{
						_displayString = c->GetTakeId() + " can't be carried";
					}
				}
				else
				{
					_displayString = "You can't see " + c->GetTakeId() + " in the room. Is it in something?";
				}
			}
			else if (locInv->HasEntity(c->GetFromId()))
			{
				Entity* fromEntity = locInv->GetEntity(c->GetFromId());
				if (fromEntity->HasComponent("INVENTORY"))
				{
					InventoryComponent* fromInv = dynamic_cast<InventoryComponent*>(fromEntity->GetComponent("INVENTORY"));
					if (fromInv->HasEntity(c->GetTakeId()))
					{
						if (fromInv->GetEntity(c->GetTakeId())->HasComponent("CARRYABLE"))
						{

							playerInv->AddEntity(fromInv->TakeEntity(c->GetTakeId()));
							_displayString = c->GetTakeId() + " was added to your inventory!";
						}
						else
						{
							_displayString = c->GetTakeId() + " can't be carried";
						}
					}
					else
					{
						_displayString = "You can't see " + c->GetTakeId() + " in the " + c->GetFromId();
					}
				}
				else
				{
					_displayString = c->GetFromId() + " hasn't got an inventory";
				}
			}
			else if (playerInv->HasEntity(c->GetFromId()))
			{
				Entity* fromEntity = playerInv->GetEntity(c->GetFromId());
				if (fromEntity->HasComponent("(INVENTORY)"))
				{
					InventoryComponent* fromInv = dynamic_cast<InventoryComponent*>(fromEntity->GetComponent("INVENTORY"));
					if (fromInv->HasEntity(c->GetTakeId()))
					{
						playerInv->AddEntity(fromInv->TakeEntity(c->GetTakeId()));
						_displayString = c->GetTakeId() + " was added to your inventory!";
					}
					else
					{
						_displayString = "You can't see " + c->GetTakeId() + " in the " + c->GetFromId();
					}
				}
				else
				{
					_displayString = c->GetFromId() + " hasn't got an inventory";
				}
			}
			else if (playerInv->HasEntity(c->GetTakeId()))
			{
				_displayString = "You already have that item!";
			}
			else
			{
				_displayString = "Can't find container " + c->GetFromId();
			}

		}

	}

	/*
		If the command is a go command, attempt to move the player along that path. If the path doesnt exist or isnt inputted correctly, the display string is updated accordingly.

		If the path exists, it then checks the game data to see if there are any entities in the room that block the path. If no, the player is moved to the new location.
	*/
	else if (typeid(*cmd) == typeid(GoCommand))
	{
		GoCommand* c = dynamic_cast<GoCommand*>(cmd);
		if (c->GetPathId().compare("NOWHERE") == 0)
		{
			_displayString = "Where would you like to go?";
		}
		else if (_theGame->HasPath( c->GetPathId() ,_theGame->GetPlayer()->GetLocation()))
		{
			if (_theGame->GetPlayer()->GetLocation()->PathLocked(c->GetPathId()))
			{
				_displayString = "The path that way is sealed";
			}
			else
			{
				_theGame->MovePlayer(c->GetPathId());
				RefreshDefault();
				_displayString = _defaultString;
			}
		}
		else
		{
			_displayString = "There is no path that way";
		}
	}
	else
	{
		_displayString += _defaultString; // If no command was recognised (failed command), the display string is the default string.
	}
}

/*
	Outputs to the console the current display string
*/
void GameplayState::Display()
{
	cout << endl;
	if (firstDisplay)
	{
		cout << _defaultString;
		cout << endl;
		firstDisplay = false;
	}
	else 
	{
		cout << _displayString;
		cout << endl;
	}
	
}

/*
	Refreshes the default string based on the players current location
*/
void GameplayState::RefreshDefault()
{
	_defaultString = "";
	_defaultString += "You are in " + _theGame->GetPlayer()->GetLocation()->Name() + "\t" +
		_theGame->GetPlayer()->GetLocation()->FirstId() + "\n";
	_defaultString += "What would you like to do?";
}


GameplayState::~GameplayState()
{
	_theGame = NULL;
}
