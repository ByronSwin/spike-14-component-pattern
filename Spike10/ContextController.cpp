#include "ContextController.h"

using namespace std;


/*
	The state and context controller for Zorkish. Has a map that keeps pointers to all states, as well as a vairable for the current state.

	This also has control of the game data, and intialises it here.

	Constructor intialises all values for the map as well as the game data.
*/
ContextController::ContextController()
{
	_theGame = new GameData();

	_states["MAINMENU"] = new MainMenuState(vector<string>() = {"MAINMENU"});
	_states["ABOUT"] = new InformationState(vector<string>() = { "ABOUT" });
	_states["HELP"] = new InformationState(vector<string>() = { "HELP" });
	_states["SELECT"] = new SelectAdventureState(vector<string>() = { "SELECT" }, _theGame);
	_states["GAMEPLAY"] = new GameplayState(vector<string>() = { "GAMEPLAY" }, _theGame);
	_states["HIGHSCORE"] = new HighscoreState(vector<string>() = { "HIGHSCORE" });
	_states["HOF"] = new InformationState(vector<string>() = { "HOF" });

	_currentState = _states["MAINMENU"];
}

/*
	sets the state based on the passed in identifier. If the state asked for is previous, switches to previous state.

	Either way, new previous state is set.

	If no matches, no change

	@param newState identifier for the new state.
*/
void ContextController::SetState(string newState)
{
	if (_states.count(newState) )
	{
		_previousState = _currentState;
		_currentState = _states[newState];
	}
	else if (newState.compare("PREVIOUS") == 0)
	{
		State* temp = _currentState;
		_currentState = _previousState;
		_previousState = temp;
	}

}

/*
	Gets input from the current state (note, this input does not always involve the user)
*/
string ContextController::GetInput()
{
	return _currentState->GetInput();
}

/*
	Handles the latest command. If its a start or state command, switch to the state that is asked for, otherwise, pass the command
	to the current state

	@param cmd the command to handle.
*/
void ContextController::Handle(Command* cmd)
{
	if (typeid(StartCommand) == typeid(*cmd))
	{
		StartCommand* c = dynamic_cast<StartCommand*>(cmd);
		SetState(c->State());

		_currentState->Handle(cmd);
	}
	else if (typeid(StateCommand) == typeid(*cmd))
	{
		StateCommand* c = dynamic_cast<StateCommand*>(cmd);
		SetState(c->State());
		_currentState->Handle(cmd);
	}
	else 
	{
		_currentState->Handle(cmd);
	}

}

/*
	Tells the current state to display to the console
*/
void ContextController::Display()
{
	_currentState->Display();
}

/*
	Clears the game of all states
*/
ContextController::~ContextController()
{
	_states.clear();
	_theGame->~GameData();
	_theGame = NULL;
}
