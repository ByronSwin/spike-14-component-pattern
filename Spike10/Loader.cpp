#include "Loader.h"

using namespace std;

/*
	Loader class, used to load a game. Takes in a list of exisiting files that can be adventures in its constructor

	@param adventures vector of string that is the adventures that can be loaded
*/
Loader::Loader(vector<string> adventures)
{
	_adventures = adventures;

}

/*
	Loads an adventure if the parameter matches an existing adventure. The game data parameter that is passed where the data is loaded to

	Reads file line by line, formating where necessary

	@param adventure string name of adventure file
	@param GameData* the data to load the data too.
*/
void Loader::Load(string adventure, GameData* result)
{
	bool canLoad = false;
	
	for (string s : _adventures)
	{
		if (s.compare(adventure) == 0)
		{
			canLoad = true;
			break;
		}
	}

	if (canLoad)
	{

		ifstream inStream;
		string currLine;
		ComponentLoader entityLoader;
		vector<string> newIds = {};
		string name = "";
		string desc = "";


		
		cout << "Opening file..." << endl;
		inStream.open("Adventures\\" + adventure, ifstream::in);

		getline(inStream, currLine);
		int numberOfLocs = atoi(currLine.c_str());

		cout << "Loading " << numberOfLocs << " locations.." << endl;
		for (int i = 0; i < numberOfLocs; i++) // Gets the number of locations and loads each one of them (and their entities) into game data
		{
			getline(inStream, currLine);
			newIds = SplitString(currLine);
			getline(inStream, name);
			getline(inStream, desc);

			

			Location* resultLoc = new Location(newIds, name, desc);
			
			InventoryComponent* resultInv = dynamic_cast<InventoryComponent*>
				(resultLoc->GetComponent("INVENTORY"));
			
			getline(inStream, currLine);
			int numOfEntities = atoi(currLine.c_str());
			for (int i = 0; i < numOfEntities; i++)
			{
				resultInv->AddEntity(
					entityLoader.LoadEntity(inStream));
			}
			
			result->AddLocation(resultLoc);

			
			cout << "Location loaded..." << endl;
		}

		cout << "Locations complete" << endl;
		getline(inStream, currLine);
		int numberOfEdges = atoi(currLine.c_str());

		cout << "Loading "<< numberOfEdges << " connections..." << endl;

		for (int i = 0; i < numberOfEdges; i++) //Reads the number of edges and adds them to game data.
		{
			getline(inStream, currLine);
			newIds = SplitString(currLine);

			getline(inStream, currLine);

			Edge* resultEdge = new Edge(newIds, result->GetLoc(currLine));
			
			getline(inStream, currLine);

			result->AddEdge(currLine, resultEdge);
			cout << "Edge Loaded..." << endl;
		}


		cout << "Load Complete." << endl;

	}
	
}

/*
	Splits a string by the delimeter " "

	@param input the raw input
	@returns vector of strings that have been formatted
*/
vector<string> Loader::SplitString(string input)
{
	string next;
	vector<string> result;

	for (string::const_iterator it = input.begin(); it != input.end(); it++)
	{
		if (*it == ' ')
		{
			if (!next.empty())
			{
				result.push_back(next);
				next.clear();
			}
		}
		else
		{
			next += *it;
		}
	}
	if (!next.empty())
	{
		result.push_back(next);
	}

	return result;
}

Loader::~Loader()
{
}
