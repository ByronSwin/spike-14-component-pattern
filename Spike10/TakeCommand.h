#pragma once

#include "Command.h"

class TakeCommand : public Command
{
public:
	TakeCommand(std::vector<std::string> ids);

	void Update(std::vector<std::string> input);

	std::string GetErrMsg();
	std::string GetFromId();
	std::string GetTakeId();

	~TakeCommand();

private:
	std::string _takeId;
	std::string _fromId;
	std::string _errMsg;
};

