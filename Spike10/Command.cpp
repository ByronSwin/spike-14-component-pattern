#include "Command.h"

using namespace std;

/*
	Command class. Used as an abstract class for all commands, inherits from IObject so they can have identifiers.

	@param ids the ids used for this command
*/
Command::Command(vector<string> ids)
: IObject(ids)
{
}

/*
	A virtual method for commands that takes in the split input and handles it.
*/
void Command::Update(vector<string> input)
{

}

Command::~Command()
{
}
