#include "InformationState.h"

using namespace std;

/*
	A simple class used to make handeling highscore file easier
*/
class result
{
public:
	string _name;
	int _score;
};

/*
	The information state. This covers the help state, the hall of fame state and the about state. The notable features of this state is it only serves to deliver information and then 
	end. Input from the user isn't read, and as soon as the user presses enter, it goes back to the previous state.

	@ids the ids for this state. The first id is used by this state to determine what information to display.
*/
InformationState::InformationState(vector<string> ids)
: State(ids)
{
}

/*
	Waits for the user to press enter, then returns pre-set information

	@returns hard-coded input
*/
string InformationState::GetInput()
{
	string input;
	getline(cin, input);
	return "INFORMATION PREVIOUS";
}

/*
	Handles users command. Does nothing for information state.
*/
void InformationState::Handle(Command* cmd)
{
	
}

/*
	Outputs to console information based on what the first id is. If its hall of fame, read the local file for scores and output those
*/
void InformationState::Display()
{
	cout << "================================================" << endl;
	
	if (FirstId().compare("(ABOUT)") == 0)
	{
		cout << "This is Zorkish, made by Byron Mihailides" << endl;
		cout << "Enjoy!" << endl;
		
	}
	else if (FirstId().compare("(HELP)") == 0)
	{
		cout << "Gameplay commands:" << endl;
		cout << "[Look] [at] (thing) looks at the thing" << endl;
		cout << "[Look] [at] (thing) [in] (container) looks at the thing in the container" << endl;
		cout << "[go] (direction)" << endl;
		cout << "[Take] (thing) Takes the thing" << endl;
		cout << "[Take] (thing) [from] (container) takes the thing from the container" << endl;
		cout << "[Quit] Quits the game" << endl;
	}
	else if (FirstId().compare("(HOF)") == 0)
	{
		string score;
		ifstream inStream;
		inStream.open("Highscores\\TheScores.txt", ifstream::in);
		result results[5];
		for (int i = 0; i < 5; i++)
		{
			getline(inStream, results[i]._name);
			getline(inStream, score);
			results[i]._score = atoi(score.c_str());
		}
		inStream.close();

		cout << "The hall of fame scores: " << endl;
		for (int i = 0; i < 5; i++)
		{
			cout << "\n" << results[i]._name;
			cout << "\n\t" << results[i]._score;
		}
	}

	cout << "Press enter to go back" << endl;
}

InformationState::~InformationState()
{
}