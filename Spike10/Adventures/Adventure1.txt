2
DUSTROOM ROOM1 START
Dusty room
A dark room. Breathing is difficult as the layers of dust cover everything.
2
SWORD
A bronze sword
A darkened blade that shows signs of extended use.
2
WEAPON
50
10
CARRY
CHAIR
A wooden chair
A creaking and borken down chair in the corner.
0
BEDROOM ROOM2
Cosy Bedroom
A cosy room with a fireplace and a large queen-sized bed. It looks good but untouched. A chest resides nearby.
2
BED
A queen-sized bed
Red felt covers this fancy and clean bed. It looks completely unused.
0
CHEST
A unlocked chest
An opened lock sits at the opening to this wooden box
1
INVENTORY
1
SPOON
A silver spoon
A shimmering silver spoon, in which your distorted reflection can clearly be seen
0
2
NORTH
ROOM2
ROOM1
SOUTH
ROOM1
ROOM2
