#pragma once

#include "Command.h"

class GoCommand : public Command
{
public:
	GoCommand(std::vector<std::string> ids);
	void Update(std::vector<std::string> input);

	std::string GetPathId();

	~GoCommand();
private:
	std::string _outId;
};

