#pragma once

#include "Location.h"
#include "Edge.h"
#include "Player.h"

#include <vector>
#include <map>

class GameData
{
public:
	GameData();
	~GameData();

	void AddLocation(Location* newLoc);
	void AddEdge(std::string keyLoc, Edge* newEdge);
	Location* GetLoc(std::string loc);
	Player* GetPlayer();
	std::string GetCurrPaths();
	bool HasPath(std::string pathId, Location* loc);

	void MovePlayer(std::string pathId);


	void StartGame();

private:
	std::vector<Location*> _locations;
	std::map<Location*, std::vector<Edge*>> _connections;
	Player* _thePlayer;

};

