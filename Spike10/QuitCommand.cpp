#include "QuitCommand.h"

using namespace std;


/*
	Quit command that inherits from command. It serves to inform the context controller to quit the game
*/
QuitCommand::QuitCommand(vector<string> ids)
:Command(ids)
{
}

QuitCommand::~QuitCommand()
{
}

