#pragma once

#include "Command.h"

class FailedCommand : public Command
{
public:
	FailedCommand(std::vector<std::string> ids);
	~FailedCommand();
};

