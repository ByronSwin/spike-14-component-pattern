#pragma once

#include "Component.h"

class LockComponent : public Component
{
public:
	LockComponent(std::string lockVal, std::string pathVal);
	~LockComponent();

	void Recieve(Message* toRec);
	bool isLocked();

	std::string CompDesc();

	std::string GetPathVal();

private:
	void Unlock(std::string keyVal);
	std::string _lockVal;
	std::string _pathVal;
	bool _locked;
};

